#!/usr/bin/perl
# (c) 2004 Christoph Berg <cb@df7cb.de>
# This script is free software covered by the GNU GPL.
# cb 2004-05-29: initial version.
# cb 2004-07-10: added umask.

# Wrapper to call usemod wiki. If you want to use more than one wiki instance,
# you can copy (and rename) this script and adjust $ConfigFile below.

# NOTE: Do not use this wrapper with mod_perl! It will throw lots of warnings
# and probably break.

# Load usemod wiki.
local $_ = "nocgi";
do "/usr/lib/cgi-bin/wiki.pl";

# If the config file is in the same directory as this file, you can just give
# the file name here, as Apache chdirs here while serving the request.
local $UseModWiki::ConfigFile;
$UseModWiki::ConfigFile = "config";

# If your config file does not include $DataDir, define it here.
#local $UseModWiki::DataDir;
#$UseModWiki::DataDir = "/var/lib/usemod-wiki";

# Make created files and directories group-writable. Useful if your webserver
# runs under a different uid than your files.
umask 0002;

# Call wiki.
UseModWiki::DoWikiRequest();
